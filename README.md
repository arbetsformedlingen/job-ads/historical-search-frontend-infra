# Historical-frontend-infra

This repository contains files regarding build and deployment
pipelines for the [Historical search frontend](https://gitlab.com/arbetsformedlingen/job-ads/historical-search-frontend)

## Directories

In the examples directory are template files to for needed secrets.

The argoocd directory contains files to activate ArgoCD deployments.
